<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('menu_id')->constrained();
            $table->string('kode_order');
            $table->string("nama_pelanggan");
            $table->integer("qty");
            $table->integer("subtotal");
            $table->integer("jumlah");
            $table->enum('status', [1, 2, 3, 4])->default(1)->comment = "1 = dapur, 2 = selesai masak, 3 = selesai";
            $table->enum('tipe', [1, 2])->comment = "1 = bawa pulang, 2 = makan tempat";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
