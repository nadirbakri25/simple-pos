<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->string("hp");
            $table->date("tanggal");
            $table->string("jam");
            $table->integer("jumlah");
            $table->enum("claim", [1, 2])->default(1)->comment = "1 = belum, 2 = sudah";
            $table->string("midtrans_id")->nullable();
            $table->enum('status', [1, 2, 3, 4])->default(2)->comment = "1 = confirmed, 2 = pending, 3 = failed, 4 = expired";
            $table->string('snap_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
