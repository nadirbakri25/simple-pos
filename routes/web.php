<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderContoller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (session()->has('success')) {
        $pesan = session('success');
        Alert::success('Sukses!', $pesan);
    }

    return view('welcome');
});

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::get('/verif-email/{username}', [RegisterController::class, 'verif']);
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate'])->middleware('guest');
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/reservasi', [ReservationController::class, 'index'])->middleware('auth');
Route::get('/reservasi/store', [ReservationController::class, 'store'])->middleware('auth');

Route::get('/invoice', [InvoiceController::class, 'index'])->middleware('auth');
Route::get('/invoice/detail/{id}', [InvoiceController::class, 'show'])->middleware('auth');

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('isAdmin');

Route::get('/dashboard/reservasi/check', [ReservationController::class, 'check'])->middleware('isOwnerKasir');

Route::get('/dashboard/menu', [MenuController::class, 'index'])->middleware('isOwner');
Route::get('/dashboard/menu/redirect', [MenuController::class, 'redirect_menu'])->middleware('isOwner');
Route::get('/dashboard/menu/{id}/edit', [MenuController::class, 'edit'])->middleware('isOwner');
Route::get('/dashboard/menu/{id}/destroy', [MenuController::class, 'destroy'])->middleware('isOwner');
Route::post('/dashboard/menu/store', [MenuController::class, 'store'])->middleware('isOwner');
Route::put('/dashboard/menu/update', [MenuController::class, 'update'])->middleware('isOwner');

Route::get('/dashboard/order', [OrderContoller::class, 'index'])->middleware('isOwnerKasir');
Route::get('/dashboard/cart', [OrderContoller::class, 'cartList'])->name('cart.list')->middleware('isOwnerKasir');
Route::get('/dashboard/cart/remove/{id}', [OrderContoller::class, 'destroy'])->name('cart.remove')->middleware('isOwnerKasir'); 
Route::post('/dashboard/cart', [OrderContoller::class, 'store'])->name('cart.store')->middleware('isOwnerKasir');
Route::post('/dashboard/cart/update', [OrderContoller::class, 'update'])->name('cart.update')->middleware('isOwnerKasir');
Route::post('/dashboard/cart/clear', [OrderContoller::class, 'destroyAll'])->name('cart.clear')->middleware('isOwnerKasir');
Route::post('/dashboard/pesan', [OrderContoller::class, 'pesan'])->middleware('isOwnerKasir');

Route::get('/dashboard/order/check', [OrderContoller::class, 'check'])->middleware('isAdmin');
Route::post('/dashboard/order/check/ganti/selesai/masak', [OrderContoller::class, 'selesai_masak'])->middleware('isAdmin');
Route::post('/dashboard/order/check/ganti/selesai/antar', [OrderContoller::class, 'selesai_antar'])->middleware('isAdmin');

Route::get('/dashboard/pendapatan', [OrderContoller::class, 'pendapatan'])->middleware('isOwner');
Route::post('/dashboard/pendapatan/cari', [OrderContoller::class, 'cari']);