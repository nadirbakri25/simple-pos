$(document).ready(function (e) {
    $('#path').change(function(){
        let reader = new FileReader();
        
        reader.onload = (e) => { 
            $("#preview").removeClass('d-none');
            $("#preview").css("background-image", "url(" + e.target.result + ")");
        }
        
        reader.readAsDataURL(this.files[0]); 
    });
});

$("#menuModal").submit((e) => {
    e.preventDefault();

    $('#hiddenAvail').val($('#isAvail').val());
    $('#hiddenTipe').val($('#tipe').val());

    let formData = new FormData();
    formData.append('nama', $('#nama').val());
    formData.append('tipe', $('#hiddenTipe').val());
    formData.append('harga', $('#harga').val());
    formData.append('isAvail', $('#hiddenAvail').val());
    formData.append("path",$("#path")[0].files[0]);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })

    $.ajax({
        url: "/dashboard/menu/store", 
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false
    })
    .done((data) => {
        window.location.href = "/dashboard/menu/redirect";
    })
    .fail((data) => {
        console.log(data);
        if (data.responseJSON.errors.isAvail) {
            $('#isAvail').addClass('is-invalid');
            $('#availInvalid').text(data.responseJSON.errors.isAvail);
        }
        if (data.responseJSON.errors.tipe) {
            $('#tipe').addClass('is-invalid');
            $('#tipeInvalid').text(data.responseJSON.errors.tipe);
        }
        if (data.responseJSON.errors.harga) {
            $('#harga').addClass('is-invalid');
            $('#hargaInvalid').text(data.responseJSON.errors.harga);
        }
        if (data.responseJSON.errors.nama) {
            $('#nama').addClass('is-invalid');
            $('#namaInvalid').text(data.responseJSON.errors.nama);
        }
        if (data.responseJSON.errors.path) {
            $('#path').addClass('is-invalid');
            $('#pathInvalid').text(data.responseJSON.errors.path);
        }
    });
});

$('#nama').on('input', () => {
    if ($('#nama').hasClass("is-invalid")) {
        removeInvalid($('#nama'));
    }
});

$('#harga').on('input', () => {
    if ($('#harga').hasClass("is-invalid")) {
        removeInvalid($('#harga'));
    }
});

$( "#tipe" ).change(() => {
    if ($('#tipe').hasClass("is-invalid")) {
        removeInvalid($('#tipe'));
    }
});

$( "#isAvail" ).change(() => {
    if ($('#isAvail').hasClass("is-invalid")) {
        removeInvalid($('#isAvail'));
    }
});

$( "#path" ).change(() => {
    if ($('#path').hasClass("is-invalid")) {
        removeInvalid($('#path'));
    }
});

removeInvalid = (input) => {
    input.removeClass("is-invalid");
};