$('#nama_pelanggan').keyup((e) => {
    let dt = new Date() 
    let year = String(dt.getFullYear())
    let month = String(dt.getMonth())
    let date = String(dt.getDate())
    let hour = String(dt.getHours())
    let minute = String(dt.getMinutes())
    let second = String(dt.getSeconds())
    let word = $(e.target).val()

    $('#kode_order').val(year + '/' + month + '/' + date + '/' + hour + '/' + minute + '/' + second + '/' + word.split(' ').join('_'));
});

$('#check-reservation').click(function(){
    if($(this).is(":checked")){
        $('#reservasi').prop('disabled', false);

        $('#reservasi').change(() => {
            let jumlah;

            if ($('#jumlah').val() == $('#jumlahHidden').val()) {
                jumlah = $('#jumlah').val();
            } else {
                jumlah = $('#jumlahHidden').val();
            }

            let newJumlah = jumlah.split(',')[0].split('.').join("");

            if($('#reservasi').val() == '') {
                newJumlah = parseInt(jumlah.split(',')[0].split('.').join(""));

                $('#lebih').addClass('d-none');
                $('#jumlah').val(newJumlah.toLocaleString('id-ID') + ',00');
                $('#jumlah').val($('#jumlahHidden').val());
            } else {
                if (newJumlah <= 50000) {
                    let jumlahFinal = 50000 - parseInt(newJumlah);
                    $('#jumlah').val(jumlahFinal.toLocaleString('id-ID') + ',00');
                    $('#lebih').removeClass('d-none');
                } 
                else {
                    let jumlahFinal = parseInt(newJumlah) - 50000;
                    $('#jumlah').val(jumlahFinal.toLocaleString('id-ID') + ',00');
                    if (!$('#lebih').hasClass('d-none')) {
                        $('#lebih').addClass('d-none');
                    }
                }
            }
        });
    }
    else if($(this).is(":not(:checked)")){
        let jumlah = $('#jumlahHidden').val();
        let newJumlah = parseInt(jumlah.split(',')[0].split('.').join(""));

        $("#reservasi").val('').change();
        $('#reservasi').prop('disabled', true);
        $('#jumlah').val(newJumlah.toLocaleString('id-ID') + ',00');
        $('#lebih').addClass('d-none');
    }
});