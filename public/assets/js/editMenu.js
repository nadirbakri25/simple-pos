$(document).ready(function (e) {
    $('#path').change(function(){
        let reader = new FileReader();
        
        reader.onload = (e) => { 
            $("#preview").removeClass('d-none');
            $("#preview").css("background-image", "url(" + e.target.result + ")");
        }
        
        reader.readAsDataURL(this.files[0]); 
    });
});

$('#nama').on('input', () => {
    if ($('#nama').hasClass("is-invalid")) {
        removeInvalid($('#nama'));
    }
});

$('#harga').on('input', () => {
    if ($('#harga').hasClass("is-invalid")) {
        removeInvalid($('#harga'));
    }
});

$( "#tipe" ).change(() => {
    if ($('#tipe').hasClass("is-invalid")) {
        removeInvalid($('#tipe'));
    }
});

$( "#isAvail" ).change(() => {
    if ($('#isAvail').hasClass("is-invalid")) {
        removeInvalid($('#isAvail'));
    }
});

$( "#path" ).change(() => {
    if ($('#path').hasClass("is-invalid")) {
        removeInvalid($('#path'));
    }
});

removeInvalid = (input) => {
    input.removeClass("is-invalid");
};