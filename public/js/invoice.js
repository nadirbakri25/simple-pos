$(".invoiceBtn").click(function(){
    let idReservasi = $(this).siblings('.table').children('tbody').children('tr').children('td.idReservasi').text();
    
    $.ajax({
        url: 'invoice/detail/'+idReservasi,
        type: 'get',
        dataType: 'json',
        success: function(response){
            let len = 0;
            $('#modalTable tbody').empty();

            if(response != 0){
                let id = response.id;
                let item = "Pemesanan Tempat";
                let jam = response.jam;
                let status = response.status;
                let jumlah = response.jumlah;

                let tanggal = response.tanggal;
                let [year, month, day] = tanggal.split('-');
                
                let created_at = response.created_at;
                created_at = created_at.substring(0,10);
                let [yearC, monthC, dayC] = created_at.split('-');
                
                let tr_str = 
                "<tr>" +
                    "<th>" + 1 + "</th>" +
                    "<td>" + item + "</td>" +
                    "<td>" + jam + "</td>" +
                    "<td>" + jumlah + "</td>" +
                    "<td>" + day + '/' + month + '/' + year + "</td>" +
                    "<td>" + "Rp. 50.000,00" + "</td>" +
                "</tr>";

                $("#modalTable tbody").append(tr_str);
                $("#kodeInvoice").text("INV/" + yearC + '/' + monthC + '/' + dayC + '/' + id);
                $("#tanggalInvoice").text(dayC + '/' + monthC + '/' + yearC);
                $("#idReservasiHidden").val(id);

                if (status == 1) {
                    $(".bayarBtn").removeClass('btn-primary').addClass('btn-success').prop('disabled', true).text("Pembayaran sudah terkonfirmasi");
                } else if (status == 2) {
                    $(".bayarBtn").text("Klik disini untuk membayar");
                } else if (status == 3) {
                    $(".bayarBtn").removeClass('btn-primary').addClass('btn-danger').prop('disabled', true).text("Pembayaran gagal");
                } else if (status == 4) {
                    $(".bayarBtn").removeClass('btn-primary').addClass('btn-danger').prop('disabled', true).text("Pembayaran expired");
                }

            }else{
                let tr_str = 
                "<tr>" +
                    "<td colspan='5'>No record found.</td>" +
                "</tr>";
                
                $("#modalTable tbody").append(tr_str);
                $("#kodeInvoice").text("No record");
                $("#tanggalInvoice").text("No record");
                $("#idReservasiHidden").val(0);
            }
        }
    });
});

$("#formBayar").submit((e) => {
    e.preventDefault();
    
    $.post("/api/invoice/bayar", {
        _method: 'POST',
        _token: $('#token').val(),
        id: $('#idReservasiHidden').val(),
        name: $('#name').val(),
        email: $('#email').val(),
    },
    (data, status) => {
        snap.pay(data.snap_token, {
            onSuccess: () => {
                location.reload();
            },
            onPending: () => {
                location.reload();
            },
            onError: () => {
                location.reload();
            }
        });
        return false;
    });
});