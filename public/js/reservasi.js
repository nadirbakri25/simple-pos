$('#name').on('input', () => {
    if ($('#name').hasClass("is-invalid")) {
        removeInvalid($('#name'));
    }
});

$('#hp').on('input', () => {
    if ($('#hp').hasClass("is-invalid")) {
        removeInvalid($('#hp'));
    }
});

$('#email').on('input', () => {
    if ($('#email').hasClass("is-invalid")) {
        removeInvalid($('#email'));
    }
});

$('#jumlah').on('input', () => {
    if ($('#jumlah').hasClass("is-invalid")) {
        removeInvalid($('#jumlah'));
    }
});

$( "#tanggal" ).change(() => {
    if ($('#tanggal').hasClass("is-invalid")) {
        removeInvalid($('#tanggal'));
    }
});

$( "#jam" ).change(() => {
    if ($('#jam').hasClass("is-invalid")) {
        removeInvalid($('#jam'));
    }
});

removeInvalid = (input) => {
    input.removeClass("is-invalid");
}

$(function() {
    // min="2022-06-22"
    const date = new Date();
    const lusa = new Date(date);
    lusa.setDate(lusa.getDate() + 2);
    
    $( "#tanggal" ).datepicker({
        startDate: lusa
    })
});