$('#name').on('input', () => {
    if ($('#name').hasClass("is-invalid")) {
        removeInvalid($('#name'));
    }
});

$('#username').on('input', () => {
    if ($('#username').hasClass("is-invalid")) {
        removeInvalid($('#username'));
    }
});

$('#email').on('input', () => {
    if ($('#email').hasClass("is-invalid")) {
        removeInvalid($('#email'));
    }
});

$('#password').on('input', () => {
    if ($('#password').hasClass("is-invalid")) {
        removeInvalid($('#password'));
    }
});

$('#toggle-password').click(()=> {
    if ($('#eye').hasClass("fa-eye")) {
        $('#eye').removeClass('fa-eye').addClass('fa-eye-slash');
        $('#password').prop('type', 'text');
    } else {
        $('#eye').removeClass('fa-eye-slash').addClass('fa-eye');
        $('#password').prop('type', 'password');
    }
});

$('#toggle-password-confirmation').click(()=> {
    if ($('#eye-confirmation').hasClass("fa-eye")) {
        $('#eye-confirmation').removeClass('fa-eye').addClass('fa-eye-slash');
        $('#password_confirmation').prop('type', 'text');
    } else {
        $('#eye-confirmation').removeClass('fa-eye-slash').addClass('fa-eye');
        $('#password_confirmation').prop('type', 'password');
    }
});

removeInvalid = (input) => {
    input.removeClass("is-invalid");
};