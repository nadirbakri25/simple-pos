<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 2) {
            return redirect('/dashboard/order');
        } else if (auth()->user()->role == 3) {
            return redirect('/dashboard/order/check');
        } else if (auth()->user()->role == 4) {
            return redirect('/dashboard/menu');
        } else {
            abort(403);
        }
    }
}
