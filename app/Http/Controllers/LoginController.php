<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Mail\verifyEmail;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Sukses!', $pesan);
        }
        
        if (session()->has('error')) {
            $pesan = session('error');
            Alert::error('Error!', $pesan);
        }

        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (auth()->user()->email_verified_at == null) {
                Mail::to(auth()->user()->email)->send(new verifyEmail(auth()->user()->username));
                Auth::logout();
                return back()->with('error', 'Silahkan verifikasi email!');
            }
            
            return redirect()->intended('/');
        }

        return back()->with('error', 'Login gagal!');
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/')->with('success', 'Logout berhasil!');
    }
}
