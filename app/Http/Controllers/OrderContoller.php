<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use App\Models\Reservation;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Cart;
use DateTime;
use Carbon\Carbon;

class OrderContoller extends Controller
{
    public function index()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Sukses!', $pesan);
        }

        if (session()->has('error')) {
            $pesan = session('error');
            Alert::error('Error!', $pesan);
        }
        
        $foods = Menu::whereTipe('makanan')->get();
        $drinks = Menu::whereTipe('minuman')->get();
        $carts = Cart::content();

        return view('admin.order.order', compact('foods', 'drinks', 'carts'));
    }

    public function store(Request $request)
    {
        Cart::add([
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'qty' => $request->quantity
        ]);

        return redirect('/dashboard/order')->with('success', 'Berhasil menambahkan menu!');
    }

    public function destroy($id)
    {
        $carts = Cart::content();

        foreach ($carts as $cart) {
            if ($id == $cart->id) {
                $rowId = $cart->rowId;
            }
        }

        Cart::remove($rowId);
        return redirect('/dashboard/order')->with('success', 'Berhasil menghapus menu!');
    }

    public function destroyAll()
    {
        $carts = Cart::content();

        if (empty($carts)) {
            return redirect('/dashboard/order')->with('error', 'Cart masih kosong!');
        } else {
            Cart::destroy();
            return redirect('/dashboard/order')->with('success', 'Berhasil menghapus semua cart!');
        }
    }

    public function update(Request $request)
    {
        $carts = Cart::content();

        foreach ($carts as $cart) {
            if ($request->id == $cart->id) {
                $rowId = $cart->rowId;
            }
        }

        Cart::update($rowId, $request->quantity);

        return redirect('/dashboard/order')->with('success', 'Berhasil mengubah data!');
    }

    public function cartList()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d");
        $dateCarbon = Carbon::createFromFormat('Y-m-d', $date);

        $carts = Cart::content();
        $subtotal = Cart::subtotal();
        $reservations = Reservation::where('claim', '1')->where('status', '1')->whereDate('tanggal', $dateCarbon)->get() ? : '';

        if ($carts->count() == 0) {
            return redirect('/dashboard/order')->with('error', 'Keranjang harus ada isi!');
        } else {
            return view('admin.order.cart', compact('carts', 'subtotal', 'reservations'));
        }
    }

    public function pesan(Request $request)
    {
        $validatedData = $request->validate([
            'kode_order'       => 'required',
            'nama_pelanggan'   => 'required',
            'jumlah'           => 'required',
            'tipe'           => 'required',
        ],[
            'kode_order.required'     => 'Kode order otomatis terisi jika nama terisi!',
            'nama_pelanggan.required' => 'Nama harus diisi!',
            'jumlah.required'         => 'Masukkan barang ke keranjang!',
            'tipe.required'           => 'Tipe harus dipilih!',
        ]);
        
        $carts = Cart::content();

        $validatedData['jumlah'] = $request->jumlahHidden;
        $validatedData['jumlah'] = explode(',', $validatedData['jumlah'])[0];
        $validatedData['jumlah'] = str_replace('.', '', $validatedData['jumlah']);

        foreach ($carts as $cart) {
            Order::create([
                'menu_id'        => $cart->id,
                'kode_order'     => $validatedData['kode_order'],
                'nama_pelanggan' => $validatedData['nama_pelanggan'],
                'qty'            => $cart->qty,
                'subtotal'       => ($cart->qty * $cart->price),
                'jumlah'         => $validatedData['jumlah'],
                'status'         => 1,
                'tipe'           => $validatedData['tipe'],
            ]);
        }

        Cart::destroy();

        if ($request->reservasi) {
            Reservation::find($request->reservasi)->update(['claim' => 2]);
        }

        return redirect('/dashboard/order')->with('success', 'Order berhasil masuk ke dapur!');
    }
    
    public function check()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Selamat!', $pesan);
        }

        if (session()->has('error')) {
            $pesan = session('error');
            Alert::error('Error!', $pesan);
        }
        
        date_default_timezone_set('Asia/Jakarta');
        $date = date("Y-m-d");
        
        if (auth()->user()->role == 2) {
            $orders = Order::orderBy('created_at')->whereDate('created_at', $date)->whereStatus(2)->get()->groupBy('kode_order');
            return view('admin.order.check', compact('orders'));
        } else if (auth()->user()->role == 3) {
            $orders = Order::orderBy('created_at')->whereDate('created_at', $date)->whereStatus(1)->get()->groupBy('kode_order');
            return view('admin.order.check', compact('orders'));
        } else if (auth()->user()->role == 4) {
            $orders = Order::orderBy('created_at')->whereDate('created_at', $date)->get()->groupBy('kode_order');
            return view('admin.order.check', compact('orders'));
        } else {
            abort(403);
        }
    }

    public function selesai_masak(Request $request)
    {
        Order::where('kode_order', '=', $request->kode_order)->update(['status' => 2]);

        return redirect('/dashboard/order/check')->with('success', 'Status order berhasil diubah!');
    }

    public function selesai_antar(Request $request)
    {
        Order::where('kode_order', '=', $request->kode_order)->update(['status' => 3]);

        return redirect('/dashboard/order/check')->with('success', 'Status order berhasil diubah!');
    }

    public function pendapatan()
    {
        return view('admin.pendapatan.tanggal');
    }

    public function cari(Request $request)
    {
        $validatedData = $request->validate([
            'awal'  => 'required',
            'akhir' => 'required',
        ],[
            'awal.required'   => 'Tanggal awal harus terisi!',
            'akhir.required'  => 'Tanggal akhir harus terisi!',
        ]);

        $dayAwal = (new DateTime($validatedData['awal']))->modify('-1 day')->format('Y-m-d');

        $startDate = Carbon::createFromFormat('Y-m-d', $dayAwal);
        $endDate = Carbon::createFromFormat('Y-m-d', $validatedData['akhir']);
        
        $orders = Order::where('status', 3)->whereBetween('updated_at', [$startDate, $endDate])->get()->groupBy('kode_order');
        $reservations = Reservation::where('claim', 1)->whereBetween('tanggal', [$startDate, $endDate])->get();

        return view('admin.pendapatan.pendapatan', compact('orders', 'reservations'));
    }
}
