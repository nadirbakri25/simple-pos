<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Mail\verifyEmail;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.register');
    }
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'     => 'required|max:255',
            'username' => 'required|min:4|max:255|unique:users',
            'email'    => 'required|email:dns|unique:users',
            'password' => 'required|confirmed|min:5|max:255'
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        Mail::to($validatedData['email'])->send(new verifyEmail($validatedData['username']));

        User::create($validatedData);

        return redirect('/login')->with('success', 'Registrasi Sukses! Silahkan cek email untuk melakukan verifikasi!');
    }

    public function verif($username)
    {
        $user = User::where('username', $username)->first();

        $user->update(['email_verified_at' => date("Y-m-d H:i:s")]);

        return redirect('/login')->with('success', 'Verifikasi Email Sukses! Silahkan melanjutkan login!');
    }
}
