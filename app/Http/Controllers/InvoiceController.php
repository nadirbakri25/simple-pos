<?php

namespace App\Http\Controllers;


use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;

class InvoiceController extends Controller
{
    public function __construct()
    {
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    public function index()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Sukses!', $pesan);
        }

        $reservations = User::find(auth()->user()->id)->reservations;

        return view('invoice.invoice', compact('reservations'));
    }

    public function show($id)
    {
        $reservation = Reservation::find($id);

        return json_encode($reservation);
    }

    public function store(Request $request)
    {
        $reservation = Reservation::find($request->id);

        $reservation->midtrans_id ='INV/' . uniqid();
        $reservation->save();

        $payload = [
            'transaction_details' => [
                'order_id' => $reservation->midtrans_id,
                'gross_amount' => 1,
            ],
            'customer_details' => [
                'first_name' => $request->name,
                'email' => $request->email,
            ],
            'item_details' => [
                [
                    'id' => 'BOOK-1',
                    'price' => '50000',
                    'quantity' => 1,
                    'name' => 'Pemesanan Tempat',
                ]
            ]
        ];

        $snapToken = Veritrans_Snap::getSnapToken($payload);

        $reservation->snap_token = $snapToken;
        $reservation->save();

        $this->response['snap_token'] = $snapToken;

        return response()->json($this->response);
    }

    public function notification()
    {
        $notif = new Veritrans_Notification();

        $notif = new Veritrans_Notification();
        
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $orderId = $notif->order_id;
        $fraud = $notif->fraud_status;
        $reservation = Reservation::where('midtrans_id', $orderId)->first();

        if ($transaction == 'capture') {
            if ($type == 'credit_card') {
                if($fraud == 'challenge') {
                    $reservation->setStatusPending();
                } else {
                    $reservation->setStatusSuccess();
                }
            }
        } else if ($transaction == 'settlement') {
            $reservation->setStatusSuccess();
        } else if($transaction == 'pending'){
            $reservation->setStatusPending();
        } else if ($transaction == 'deny') {
            $reservation->setStatusFailed();
        } else if ($transaction == 'expire') {
            $reservation->setStatusExpired();
        } else if ($transaction == 'cancel') {
            $reservation->setStatusFailed();
        }
        return;
    }
}
