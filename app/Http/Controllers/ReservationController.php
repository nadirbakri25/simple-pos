<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class ReservationController extends Controller
{
    public function index()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Sukses!', $pesan);
        }

        if (session()->has('error')) {
            $pesan = session('error');
            Alert::error('Error!', $pesan);
        }

        return view('reservasi.reservasi');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'hp' => 'required|numeric|starts_with:08',
            'tanggal' => 'required|date',
            'jam' => 'required',
            'jumlah' => 'required|numeric',
        ],[
            'hp.required' => 'HP harus diisi!',
            'hp.numeric' => 'Kolom ini harus angka!',
            'hp.starts_with' => 'Hp harus diawali dengan 08!',
            'tanggal.required' => 'Tanggal harus diisi!',
            'tanggal.date' => 'Kolom ini harus berisi tanggal!',
            'jam.required' => 'Jam harus dipilih!',
            'jumlah.required' => 'Jumlah harus diisi!',
            'jumlah.numeric' => 'Kolom ini harus angka!',
        ]);

        $explo = explode('/', $validatedData['tanggal']);
        $validatedData['tanggal'] = strval($explo[2]) . '-' .strval($explo[0]) . '-'. strval($explo[1]);
        
        $countReservation = Reservation::where('tanggal', $validatedData['tanggal'])->where('jam', $validatedData['jam'])->whereNotIn('status', [3, 4])->count();
        
        if ($countReservation > 5) {
            return back()->with('error', 'Gagal! Tempat sudah penuh!');
        }

        $validatedData = array("user_id" => auth()->user()->id) + $validatedData;

        Reservation::create($validatedData);
        
        return redirect('/invoice')->with('success', 'Pemesanan sukses! Silahkan lanjutkan pembayaran pada halamam invoice!');
    }

    public function check()
    {
        if (auth()->user()->role == 2) {
            date_default_timezone_set('Asia/Jakarta');
            $date = date("Y-m-d");
            $dateCarbon = Carbon::createFromFormat('Y-m-d', $date);

            $datas = Reservation::orderBy('created_at')->whereClaim(1)->whereStatus(1)->whereDate('tanggal', $dateCarbon)->get();
            return view('admin.reservasi.reservasi', compact('datas'));
        } else if (auth()->user()->role == 4) {
            date_default_timezone_set('Asia/Jakarta');
            $date = date("Y-m-d");
            $dateCarbon = Carbon::createFromFormat('Y-m-d', $date);
            
            $datas = Reservation::orderBy('created_at')->whereDate('tanggal', $dateCarbon)->get();
            return view('admin.reservasi.reservasi', compact('datas'));
        } else {
            abort(403);
        }
    }
}
