<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Order;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class MenuController extends Controller
{
    public function index()
    {
        if (session()->has('success')) {
            $pesan = session('success');
            Alert::success('Sukses!', $pesan);
        }

        if (session()->has('error')) {
            $pesan = session('error');
            Alert::error('Error!', $pesan);
        }
        
        $foods = Menu::whereTipe('makanan')->get();
        $drinks = Menu::whereTipe('minuman')->get();

        return view('admin.menu.menu', compact('foods', 'drinks'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama'   => 'required|min:3',
            'tipe'   => 'required',
            'harga'  => 'required|numeric',
            'isAvail'  => 'required|not_in:2',
            'path' => 'required|image',
        ],[
            'nama.required' => 'Nama harus diisi!',
            'nama.min' => 'Nama minimal harus 3 karakter!',
            'tipe.required' => 'Tipe harus dipilih!',
            'harga.required' => 'Harga harus diisi!',
            'harga.numeric' => 'Harga harus numeric!',
            'isAvail.required' => 'Available harus dipilih!',
            'path.required' => 'Gambar harus diisi!',
            'path.image' => 'File harus sebuah gambar!',
        ]);

        $image = $request->file('path');
        $imageName = time(). ".". $image->getClientOriginalName();
        $validatedData['path'] = $imageName;
        $image->move(public_path('menu'), $imageName);

        Menu::create($validatedData);
        
        return json_encode(['message' => 'Sukses!']);
    }

    public function edit($id)
    {
        $menu = Menu::find($id);

        return view('admin.menu.edit', compact('menu'));
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'id'      => 'required',
            'nama'    => 'required|min:3',
            'tipe'    => 'required',
            'harga'   => 'required|numeric',
            'isAvail' => 'required|not_in:2',
            'path'    => 'image',
        ],[
            'nama.required' => 'Nama harus diisi!',
            'nama.min' => 'Nama minimal harus 3 karakter!',
            'tipe.required' => 'Tipe harus dipilih!',
            'harga.required' => 'Harga harus diisi!',
            'harga.numeric' => 'Harga harus numeric!',
            'isAvail.required' => 'Available harus dipilih!',
            'path.image' => 'File harus sebuah gambar!',
        ]);

        $menu = Menu::find($request->id);
        
        if (array_key_exists('path', $validatedData)) {
            unlink('menu/' . $menu->path);
            
            $image = $request->file('path');
            $imageName = time(). ".". $image->getClientOriginalName();
            $validatedData['path'] = $imageName;
            $image->move(public_path('menu'), $imageName);            
        } else {
            $validatedData['path'] = $menu->path;
        }

        $menu->update($validatedData);

        return redirect('/dashboard/menu')->with('success', 'Update Berhasil!');
    }

    public function destroy($id)
    {
        $menu = Menu::find($id);
        $orders = Order::where('menu_id', $id)->first();
        
        if ($orders !== null) {
            return redirect('/dashboard/menu')->with('error', 'Gagal! Menu sudah memiliki riwayat pemesanan, silahkan mengubah status ketersediaannya jika ingin disable menu');
        } else {
            unlink('menu/' . $menu->path);
    
            $menu->delete();
    
            return redirect('/dashboard/menu')->with('success', 'Delete Berhasil!');
        }
    }

    public function redirect_menu()
    {
        return redirect('/dashboard/menu')->with('success', 'Pembuatan menu sukses!');
    }
}
