<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class verifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username)
    {
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@pindangombai.com', 'Pindang Ombai')
        ->subject('Verifikasi Email')
        ->markdown('mails.verifyEmail')
        ->with([
            'name' => $this->username,
            'link' => URL::to('/') . '/verif-email/' . $this->username
        ]);    
    }
}
