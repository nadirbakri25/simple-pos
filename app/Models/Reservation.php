<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'hp',
        'tanggal',
        'jam',
        'jumlah',
        'claim',
        'midtrans_id',
        'status',
        'snap_token',
    ];
    
    public function setStatusPending()
    {
        $this->attributes['status'] = 2;
        self::save();
    }
    
    public function setStatusSuccess()
    {
        $this->attributes['status'] = 1;
        self::save();
    }
    
    public function setStatusFailed()
    {
        $this->attributes['status'] = 3;
        self::save();
    }

    public function setStatusExpired()
    {
        $this->attributes['status'] = 4;
        self::save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
