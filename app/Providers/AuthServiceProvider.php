<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('kasir', function(User $user)
        {
            return $user->role == '2';
        });
        Gate::define('dapur', function(User $user)
        {
            return $user->role == '3';
        });
        Gate::define('owner', function(User $user)
        {
            return $user->role == '4';
        });
        Gate::define('ownerkasir', function(User $user)
        {
            return ($user->role == '4' || $user->role == '2');
        });
        Gate::define('admin', function(User $user)
        {
            return ($user->role != '1');
        });
    }
}
