@extends('layouts.main')

@section('title', 'Register')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style.css')  }}">
    <link rel="stylesheet" href="{{ asset('css/auth.css')  }}">
@endpush

@section('content')
    <div class="container-fluid dark-bg min-vh-100 ">
        <div class="row center-item min-vh-100">
            <div class="col-12 col-sm-8 col-md-5 col-lg-4">
                <div class="card light-bg dark-text">
                    <div class="card-body">
                        <h1 class="handwriting">Register</h1>
                        <div class="underline"></div>
                        <div class="underline"></div>
                        <form class="mt-3" action="/register" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Masukkan nama panjang" value="{{ old('name') }}">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Masukkan username" value="{{ old('username') }}">
                                @error('username')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukkan email" value="{{ old('email') }}">
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Masukkan password">
                                    <div id="toggle-password" class="input-group-append">
                                        <span class="input-group-text">
                                            <i id="eye" class="fa fa-eye"></i>
                                        </span>
                                    </div>
                                    @error('password')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Konfirmasi password">
                                    <div id="toggle-password-confirmation" class="input-group-append">
                                        <span class="input-group-text">
                                            <i id="eye-confirmation" class="fa fa-eye"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-outline-dark btn-block">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/auth.js')  }}"></script>
@endpush