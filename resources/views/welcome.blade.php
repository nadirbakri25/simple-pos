@extends('layouts.main')

@section('title', 'Beranda')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style.css')  }}">
    <link rel="stylesheet" href="{{ asset('css/homepage.css')  }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 dark-bg height-100vh">
                <div class="center-text">
                    <h1 class="handwriting">Rumah Makan<br/>Pindang Ombai</h1>
                    <div class="underline"></div>
                    <div class="underline"></div>
                    <p class="lato mt-4">Rumah Makan Pindang Ombai hadir untuk memenuhi kebutuhan konsumen guna menunjang kebutuhan pokok sehari-hari. Rumah Makan Pindang Ombai berlokasi di Kota Prabumulih dan kami selalu berusaha untuk menjadi yang terbaik di dunia kuliner.</p>
                    <a href="/reservasi" type="button" class="btn btn-outline-light btn-block">Reservasi</a>
                </div>
            </div>
            <div class="col-12 col-md-6 img-bg height-100vh"></div>
        </div>
    </div>

    <div class="light-bg" id="menu">
        <div class="container">
            <h1 class="handwriting pt-5 pb-5">Our Menu</h1>
            <div class="row">
                <div class="col-6 col-md-4 mb-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-image"></div>
                            <h5 class="card-title mt-3" style="font-weight: 700;">Pindang Patin</h5>
                            <p class="card-text">Rp. 10.000,00</p>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-image" style="background-image: url('/images/tulang.jpg')"></div>
                            <h5 class="card-title mt-3" style="font-weight: 700;">Pindang Tulang</h5>
                            <p class="card-text">Rp. 15.000,00</p>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4 mb-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-image" style="background-image: url('/images/nila.jpg')"></div>
                            <h5 class="card-title mt-3" style="font-weight: 700;">Pindang Nila</h5>
                            <p class="card-text">Rp. 10.000,00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="dark-bg" style="position: relative; height: 54px;">
        <p class="center-text lato">Rumah Makan Pindang Ombai © 2021</p>
    </footer>
@endsection