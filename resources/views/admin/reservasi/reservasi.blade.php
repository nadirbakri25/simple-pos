@extends('layouts.admin')

@section('title', 'Check Status')

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
        <div class="breadcrumb-item">Reservasi</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Check Reservasi</h2>
    <p class="section-lead">Halaman ini akan menampilkan reservasi yang ada</p>
</div>
@endsection

@section('content')
    <div class="row">
        @foreach ($datas as $data)
        <div class="col-12 col-sm-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">Reservasi {{ $data->user->name }}</h5>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Tanggal<span class="float-right">{{ $data->tanggal }}</span></li>
                        <li class="list-group-item">Jam<span class="float-right">{{ $data->jam }}</span></li>
                        <li class="list-group-item">Claim
                            <span class="float-right">
                                @if ($data->claim == 2)
                                    Sudah
                                @else
                                    Belum                                
                                @endif
                            </span>
                        </li>
                        <li class="list-group-item">Status
                            <span class="float-right">
                                @if ($data->status == 1)
                                Confirmed
                                @elseif ($data->status == 2)
                                    Pending
                                @elseif ($data->status == 3)
                                    Failed
                                @elseif ($data->status == 4)
                                    Expired
                                @endif
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection