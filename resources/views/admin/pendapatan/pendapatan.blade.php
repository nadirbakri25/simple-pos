@extends('layouts.admin')

@section('title', 'Pendapatan')

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="/dashboard/pendapatan">Tanggal</a></div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/pendapatan.css')  }}">
@endpush

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Pendapatan Order</h2>
    <p class="section-lead">Berikut ini merupakan pendapatan yang didapatkan dari order pada tanggal yang dipilih</p>
</div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kode Order</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Nama Pelanggan</th>
                        <th scope="col">Pendapatan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $order[0]->kode_order }}</td>
                            <td>{{ $order[0]->updated_at->toDateString(); }}</td>
                            <td>{{ $order[0]->nama_pelanggan }}</td>
                            <td>Rp. {{ number_format($order[0]->jumlah, 2, ',', '.') }}</td>
                        </tr>                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="section-title-lead">
        <h2 class="section-title">Pendapatan Reservasi</h2>
        <p class="section-lead">Berikut ini merupakan pendapatan dari reservasi yang belum di claim pada tanggal yang dipilih</p>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kode Invoice</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Nama Pelanggan</th>
                        <th scope="col">Pendapatan</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($reservations->isNotEmpty())
                        @foreach ($reservations as $reservation)
                            @php
                                $tanggal = explode('-', $reservation->tanggal);
                            @endphp
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ "INV/" . $tanggal[0] . '/' . $tanggal[1] . '/' . $tanggal[2] . '/' . $reservation->id }}</td>
                                <td>{{ $reservation->tanggal }}</td>
                                <td>{{ $reservation->user->name }}</td>
                                <td>Rp. {{ number_format(50000, 2, ',', '.') }}</td>
                            </tr>                        
                        @endforeach
                    @else
                        <tr>
                            <td scope="row" colspan="5" align="center">Tidak ada data!</td>
                        </tr> 
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection