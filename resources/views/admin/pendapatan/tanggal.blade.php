@extends('layouts.admin')

@section('title', 'Pendapatan')

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
        <div class="breadcrumb-item">Pendapatan</div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/pendapatan.css')  }}">
@endpush

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Tanggal</h2>
    <p class="section-lead">Masukkan tanggal pendapatan yang ingin dilihat</p>
</div>
@endsection

@section('content')
    <form action="/dashboard/pendapatan/cari" method="POST" id="tanggal">
        @csrf
        <div class="form-group">
            <label for="awal">Tanggal Awal</label>
            <input type="date" class="form-control @error('awal') is-invalid @enderror" id="awal" name="awal" value="">
            @error('awal')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="akhir">Tanggal Akhir</label>
            <input type="date" class="form-control @error('akhir') is-invalid @enderror" id="akhir" name="akhir" value="">
            @error('akhir')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block float-right">Submit</button>
@endsection