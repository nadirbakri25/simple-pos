@extends('layouts.admin')

@section('title', 'Cart')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/order.css')  }}">
@endpush

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="/dashboard/order">Order</a></div>
        <div class="breadcrumb-item">Cart</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Cart</h2>
    <p class="section-lead">Silahkan melakukan konfirmasi kepada pelanggan</p>
</div>
@endsection

@section('content')
    <div class="row">
        @foreach ($carts as $cart)
        <div class="col-12 col-sm-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">{{ $cart->name }}</h5><hr>
                    <div class="form-group mx-auto">
                        <label for="qty">Qty</label>
                        <input id="qty" type="text" name="qty" class="form-control" value="{{ $cart->qty }}" readonly>
                    </div>
                    <div class="form-group mx-auto">
                        <label for="Subtotal_cart">Subtotal</label>
                        <input id="Subtotal_cart" type="text" name="Subtotal_cart" class="form-control" value="Rp. {{ number_format(($cart->price * $cart->qty), 2, ',', '.') }}" readonly>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <div class="section-title-lead">
        <h2 class="section-title">Data Pelanggan</h2>
        <p class="section-lead">Silahkan mengisi data pelanggan untuk melanjutkan pemesanan</p>
    </div>

    <form action="/dashboard/pesan" method="POST">
        @csrf
        <div class="form-group">
            <label for="kode_order">Kode Order</label>
            <input type="text" class="form-control @error('kode_order') is-invalid @enderror" name="kode_order" id="kode_order" value="" readonly>
            @error('kode_order')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama_pelanggan">Nama <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_pelanggan') is-invalid @enderror" name="nama_pelanggan" id="nama_pelanggan" value="{{ old('nama_pelanggan') }}">
            @error('nama_pelanggan')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tipe">Tipe Pemesanan <span class="text-danger">*</span></label>
            <select class="custom-select @error('tipe') is-invalid @enderror" name="tipe" id="tipe">
                <option selected disabled>Klik disini untuk memilih tipe</option>
                <option value="1">Bawa pulang</option>
                <option value="2">Makan di tempat</option>
            </select>
            @error('tipe')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jumlah">Jumlah</label>
            <input type="hidden" class="form-control" name="jumlahHidden" id="jumlahHidden" value="{{ $subtotal }}" readonly>
            <input type="text" class="form-control @error('jumlah') is-invalid @enderror" name="jumlah" id="jumlah" value="{{ $subtotal }}" readonly>
            <small class="text-dark d-none" id="lebih">Jika text ini muncul, maka uang pembeli berlebih sebanyak nominal diatas</small>
            @error('jumlah')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="check-reservation">
                <label class="custom-control-label" for="check-reservation">Klik jika melakukan reservasi</label>
            </div>
        </div>
        <div class="form-group">
            <label for="reservasi">Reservasi</label>
            <select class="custom-select @error('reservasi') is-invalid @enderror" name="reservasi" id="reservasi" disabled>
                <option selected value="">Klik disini untuk memilih reservasi</option>
                @foreach ($reservations as $reservation)
                    <option value="{{ $reservation->id }}">{{ $reservation->user->name }}</option>
                @endforeach
            </select>
            @error('reservasi')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-success btn-block">Bayar</button><br/>
    </form>

@endsection

@push('script')
<script type="text/javascript" src="{{ asset('assets/js/cart.js')  }}"></script>
@endpush