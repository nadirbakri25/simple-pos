@extends('layouts.admin')

@section('title', 'Order')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/order.css')  }}">
@endpush

@push('meta')
    <meta name="_token" content="{{ csrf_token() }}" />
@endpush

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item">Order</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Makanan</h2>
    <p class="section-lead">Silahkan memilih makanan yang dipesan pelanggan</p>
</div>
@endsection

@section('content')
    <div class="row">
        @foreach ($foods as $food)
        <div class="col-12 col-sm-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-header">
                    <div class="imagePreview" style="background-image: url({{ asset("menu/$food->path") }})"></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">{{ $food->nama }}</h5>
                    <p class="card-text">Rp. {{ number_format($food->harga, 2, ',', '.') }}</p>
                    <div class="input-group mx-auto">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
                                <span class="fa fa-minus"></span>
                            </button>
                        </span>
                        <input id="qty" type="text" name="quant[2]" class="form-control input-number" value="1" min="1" max="100">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
                                <span class="fa fa-plus"></span>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="card-footer bg-whitesmoke">
                    @if ($food->isAvail == 0)
                        <button class="btn btn-dark btn-block" disabled>Tidak Tersedia</button>
                    @else
                        <form id="formCart" action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{ $food->id }}" name="id">
                            <input type="hidden" value="{{ $food->nama }}" name="name">
                            <input type="hidden" value="{{ $food->harga }}" name="price">
                            <input type="hidden" value="1" name="quantity" id="quantity">
                            @if ($carts->where('id', $food->id)->count())
                                <button class="btn btn-secondary btn-block" disabled>Sudah dipesan</button>
                            @else
                                <button class="btn btn-primary btn-block">Masukkan ke keranjang</button>
                            @endif
                        </form>

                        @if ($carts->where('id', $food->id)->count())
                            <form action="{{ route('cart.update') }}" method="POST" id="formEdit">
                                @csrf
                                <input type="hidden" value="{{ $food->id }}" name="id">
                                <input type="hidden" value="1" name="quantity" id="quantityEdit">
                                <div class="btn-group d-flex" role="group" aria-label="Basic example">
                                    <button type="submit" class="btn btn-success"><i class='fas fa-pencil-alt'></i> Edit Qty</button>
                                    <a href="/dashboard/cart/remove/{{ $food->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus dari Keranjang</a>
                                </div>
                            </form>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <div class="section-title-lead" style="margin-top: -50px">
        <h2 class="section-title">Minuman</h2>
        <p class="section-lead">Silahkan memilih minuman yang dipesan pelanggan</p>
    </div>

    <div class="row">
        @foreach ($drinks as $drink)
        <div class="col-12 col-sm-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-header">
                    <div class="imagePreview" style="background-image: url({{ asset("menu/$drink->path") }})"></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">{{ $drink->nama }}</h5>
                    <p class="card-text">Rp. {{ number_format($drink->harga, 2, ',', '.') }}</p>
                    <div class="input-group mx-auto">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[2]">
                                <span class="fa fa-minus"></span>
                            </button>
                        </span>
                        <input id="qty" type="text" name="quant[2]" class="form-control input-number" value="1" min="1" max="100">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
                                <span class="fa fa-plus"></span>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="card-footer bg-whitesmoke">
                    @if ($drink->isAvail == 0)
                        <button class="btn btn-dark btn-block" disabled>Tidak Tersedia</button>
                    @else
                        <form id="formCart" action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{ $drink->id }}" name="id">
                            <input type="hidden" value="{{ $drink->nama }}" name="name">
                            <input type="hidden" value="{{ $drink->harga }}" name="price">
                            <input type="hidden" value="1" name="quantity" id="quantity">
                            @if ($carts->where('id', $drink->id)->count())
                                <button class="btn btn-secondary btn-block" disabled>Sudah dipesan</button>
                            @else
                                <button class="btn btn-primary btn-block">Masukkan ke keranjang</button>
                            @endif
                        </form>
                        
                        @if ($carts->where('id', $drink->id)->count())
                            <form action="{{ route('cart.update') }}" method="POST" id="formEdit">
                                @csrf
                                <input type="hidden" value="{{ $drink->id }}" name="id">
                                <input type="hidden" value="1" name="quantity" id="quantityEdit">
                                <div class="btn-group d-flex" role="group" aria-label="Basic example">
                                    <button type="submit" class="btn btn-success"><i class='fas fa-pencil-alt'></i> Edit Qty</button>
                                    <a href="/dashboard/cart/remove/{{ $drink->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus dari Keranjang</a>
                                </div>
                            </form>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <a href="/dashboard/cart" class="btn btn-success btn-block">Konfirmasi ({{ $carts->count() }})</a><br/>

    <form action="{{ route('cart.clear') }}" method="POST">
        @csrf
        <div class="form-group">
            <button class="btn btn-danger btn-block">Hapus semua</button>
        </div>
    </form>
@endsection

@push('script')
<script type="text/javascript" src="{{ asset('assets/js/order.js')  }}"></script>
@endpush