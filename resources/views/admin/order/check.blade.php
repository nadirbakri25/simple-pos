@extends('layouts.admin')

@section('title', 'Check Status')

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="/dashboard/order">Order</a></div>
        <div class="breadcrumb-item">Check</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Check Status Order</h2>
    <p class="section-lead">Silahkan mengganti status order jika telah selesai.</p>
</div>
@endsection

@section('content')
    <div class="row">
        @foreach ($orders as $order)
        <div class="col-12 col-sm-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">Pesanan {{ $order[0]->nama_pelanggan }} 
                        <span class="float-right badge badge-pill badge-primary small">
                            @if ($order[0]->status == 1)
                                Di dapur
                            @elseif ($order[0]->status == 2)
                                Selesai Masak
                            @elseif ($order[0]->status == 3)
                                Order selesai
                            @endif
                        </span>
                    </h5>
                    <p class="card-text">Kode: {{ $order[0]->kode_order }}</p>
                    <p class="card-text">Tipe: @if ($order[0]->tipe == 1) Bawa Pulang @else Makan di Tempat @endif</p>
                    <ul class="list-group list-group-flush">
                        @foreach ($order as $item)
                            <li class="list-group-item">{{ $item->menu->nama }} <span class="float-right">x {{ $item->qty }}</span></li>
                        @endforeach
                    </ul>
                        @if ($order[0]->status == 1)
                            <form action="/dashboard/order/check/ganti/selesai/masak" method="post">
                                @csrf
                                <input type="hidden" name="kode_order" value="{{ $order[0]->kode_order }}">
                                <button type="submit" class="btn btn-primary btn-block">Klik jika telah selesai memasak</button>
                            </form>
                        @elseif ($order[0]->status == 2)
                            <form action="/dashboard/order/check/ganti/selesai/antar" method="post">
                                @csrf
                                <input type="hidden" name="kode_order" value="{{ $order[0]->kode_order }}">
                                <button type="submit" class="btn btn-primary btn-block">Klik jika telah selesai mengantar</button>
                            </form>
                        @elseif ($order[0]->status == 3)
                            <button type="submit" class="btn btn-primary btn-block disabled">Pesanan selesai</button>
                        @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection