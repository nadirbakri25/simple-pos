@extends('layouts.admin')

@section('title', 'Menu')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/menu.css')  }}">
@endpush

@push('meta')
    <meta name="_token" content="{{ csrf_token() }}" />
@endpush

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item">Menu</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Makanan</h2>
    <p class="section-lead">Berikut ini merupakan makanan yang disediakan Rumah Makan Pindang Ombai</p>
</div>
@endsection

@section('content')
<button class="btn btn-primary float" id="tambahMenuModal" data-toggle="modal" data-target="#tambahMenu"><i class="fa fa-plus"></i></button>
    <div class="row">
        @foreach ($foods as $food)
        <div class="col-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-header">
                    <div class="imagePreview" style="background-image: url({{ asset("menu/$food->path") }})"></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">{{ $food->nama }}</h5>
                    <p class="card-text">Rp. {{ number_format($food->harga, 2, ',', '.') }}</p>
                </div>
                <div class="card-footer bg-whitesmoke">
                    @if ($food->isAvail == 0)
                        <button class="btn btn-dark btn-block" disabled>Tidak Tersedia</button>
                    @else
                        <button class="btn btn-primary btn-block" disabled>Tersedia</button>
                    @endif
                    <div class="btn-group btn-block" role="group">
                        <a href="/dashboard/menu/{{ $food->id }}/edit" type="button" class="btn btn-success">Update</a>
                        <a href="/dashboard/menu/{{ $food->id }}/destroy" type="submit" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <div class="section-title-lead">
        <h2 class="section-title">Minuman</h2>
        <p class="section-lead">Berikut ini merupakan minuman yang disediakan Rumah Makan Pindang Ombai</p>
    </div>

    <div class="row">
        @foreach ($drinks as $drink)
        <div class="col-6 col-md-4 mb-5">
            <div class="card">
                <div class="card-header">
                    <div class="imagePreview" style="background-image: url({{ asset("menu/$drink->path") }})"></div>
                </div>
                <div class="card-body">
                    <h5 class="card-title mt-3" style="font-weight: 700;">{{ $drink->nama }}</h5>
                    <p class="card-text">Rp. {{ number_format($drink->harga, 2, ',', '.') }}</p>
                </div>
                <div class="card-footer bg-whitesmoke">
                    @if ($drink->isAvail == 0)
                        <button class="btn btn-dark btn-block" disabled>Tidak Tersedia</button>
                    @else
                        <button class="btn btn-primary btn-block" disabled>Tersedia</button>
                    @endif
                    <div class="btn-group btn-block" role="group">
                        <a href="/dashboard/menu/{{ $drink->id }}/edit" type="button" class="btn btn-success">Update</a>
                        <a href="/dashboard/menu/{{ $drink->id }}/destroy" type="submit" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection

@push('modal')
<div class="modal modal_outer right_modal fade" id="tambahMenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" >
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
            <h2 class="modal-title">Tambah Menu</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form class="container" action="/dashboard/menu/store" method="POST" id="menuModal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nama">Nama Menu <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama menu" value="{{ old('nama') }}">
                        <div class="invalid-feedback" id="namaInvalid"></div>
                    </div>
                    <div class="form-group">
                        <label for="tipe">Tipe <span class="text-danger">*</span></label>
                        <select class="custom-select" name="tipe" id="tipe">
                            <option selected disabled value="2">Klik disini untuk memilih tipe</option>
                            <option value="makanan">Makanan</option>
                            <option value="minuman">Minuman</option>
                        </select>
                        <input type="hidden" name="hiddenTipe" id="hiddenTipe">
                        <div class="invalid-feedback" id="tipeInvalid"></div>
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="harga" placeholder="Masukkan harga" name="harga">
                        <div class="invalid-feedback" id="hargaInvalid"></div>
                    </div>
                    <div class="form-group">
                        <label for="avail">Apakah Tersedia? <span class="text-danger">*</span></label>
                        <select class="custom-select" name="isAvail" id="isAvail">
                            <option selected disabled value="2">Klik disini untuk memilih status menu</option>
                            <option value="1">Iya</option>
                            <option value="0">Tidak</option>
                        </select>
                        <input type="hidden" name="hiddenAvail" id="hiddenAvail">
                        <div class="invalid-feedback" id="availInvalid"></div>
                    </div>
                    <div class="form-group">
                        <label for="path">Gambar <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="path" placeholder="Masukkan Gambar" id="path">
                        <div class="invalid-feedback" id="pathInvalid"></div>
                        <div class="imagePreview d-none" id="preview"></div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endpush

@push('script')
<script type="text/javascript" src="{{ asset('assets/js/menu.js')  }}"></script>
@endpush