@extends('layouts.admin')

@section('title', 'Edit Menu')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/menu.css')  }}">
@endpush

@section('breadcrumb')
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item">Menu</div>
        <div class="breadcrumb-item">Edit</div>
    </div>
@endsection

@section('sectionTitleLead')
<div class="section-title-lead">
    <h2 class="section-title">Edit Menu</h2>
    <p class="section-lead">Silahkan mengisi form dibawah ini untuk melakukan edit menu</p>
</div>
@endsection

@section('content')
<form action="/dashboard/menu/update" id="form" method="POST" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <input type="hidden" name="id" value="{{ $menu->id }}">
    <div class="card">
        <div class="card-header">
            <div class="imagePreview" style="background-image: url({{ asset("menu/$menu->path") }})"></div>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama Menu <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Masukkan nama menu" value="{{ $menu->nama }}">
                @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tipe">Tipe <span class="text-danger">*</span></label>
                <select class="custom-select @error('tipe') is-invalid @enderror" name="tipe" id="tipe">
                    @if ($menu->tipe == 'makanan')
                        <option value="makanan" selected>Makanan</option>
                        <option value="minuman">Minuman</option>
                    @else
                        <option value="minuman" selected>Minuman</option>
                        <option value="makanan">Makanan</option>
                    @endif
                </select>
                @error('tipe')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="harga">Harga <span class="text-danger">*</span></label>
                <input type="number" class="form-control @error('harga') is-invalid @enderror" id="harga" placeholder="Masukkan harga" name="harga" value="{{ $menu->harga }}">
                @error('harga')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="avail">Apakah Tersedia? <span class="text-danger">*</span></label>
                <select class="custom-select @error('isAvail') is-invalid @enderror" name="isAvail" id="isAvail">
                    @if ($menu->isAvail == '1')
                        <option value="1" selected>Iya</option>
                        <option value="0">Tidak</option>
                    @else
                        <option value="0" selected>Tidak</option>
                        <option value="1">Iya</option>
                    @endif
                </select>
                @error('isAvail')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="path">Gambar</label>
                <input type="file" class="form-control @error('path') is-invalid @enderror" name="path" placeholder="Masukkan Gambar" id="path">
                @error('path')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
                <div class="imagePreview d-none" id="preview"></div>
            </div>
        </div>
        <div class="card-footer bg-whitesmoke">
            <button class="btn btn-primary btn-block">Submit</button>
        </div>
    </div>
</form>
@endsection

@push('script')
<script type="text/javascript" src="{{ asset('assets/js/menu.js')  }}"></script>
@endpush