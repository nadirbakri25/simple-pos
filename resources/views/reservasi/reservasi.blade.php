@extends('layouts.main')

@section('title', 'Reservasi')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style.css')  }}">
    <link rel="stylesheet" href="{{ asset('css/reservasi.css')  }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
@endpush

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-6 dark-bg height-100vh overflow-auto">
            <div class="center-text">
                <h1 class="handwriting">Rumah Makan<br/>Pindang Ombai</h1>
                <div class="underline"></div>
                <div class="underline"></div>
                <p class="lato mt-4">Silahkan mengisi formulir yang telah disediakan</p>
            </div>
        </div>
        <div class="col-12 col-md-6 light-bg dark-text height-100vh overflow-auto">
            <form class="center-text-horizontal w-75" action="/reservasi/store" id="form">
                @csrf
                <h1 class="handwriting h1-title">Form Reservasi</h1>
                <div class="underline-dark"></div>
                <div class="underline-dark"></div>
                <div class="form-group mt-3">
                    <label for="name">Nama Lengkap <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Masukkan nama lengkap" value="{{ auth()->user()->name }}" readonly>
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email <span class="text-danger">*</span></label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Masukkan email" value="{{ auth()->user()->email }}" readonly>
                    @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="hp">Nomor HP <span class="text-danger">*</span></label>
                    <input type="tel" class="form-control @error('hp') is-invalid @enderror" id="hp" name="hp" placeholder="Masukkan nomor hp" value="{{ old('hp') }}">
                    <small id="error-hp" class="text-danger"></small>
                    @error('hp')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tanggal">Tanggal Reservasi <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('tanggal') is-invalid @enderror" id="tanggal" name="tanggal" value="{{ old('tanggal') }}" placeholder="mm/dd/yyyy" autocomplete="off">
                    @error('tanggal')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="jumlah">Jam Reservasi <span class="text-danger">*</span></label>
                    <select class="custom-select @error('jam') is-invalid @enderror" name="jam" id="jam">
                        <option selected disabled>Klik disini untuk memilih jam</option>
                        <option value="08.00-09.00">08.00-09.00</option>
                        <option value="09.00-10.00">09.00-10.00</option>
                        <option value="10.00-11.00">10.00-11.00</option>
                        <option value="11.00-12.00">11.00-12.00</option>
                        <option value="12.00-13.00">12.00-13.00</option>
                        <option value="13.00-14.00">13.00-14.00</option>
                        <option value="14.00-15.00">14.00-15.00</option>
                        <option value="15.00-16.00">15.00-16.00</option>
                        <option value="16.00-17.00">16.00-17.00</option>
                        <option value="17.00-18.00">17.00-18.00</option>
                        <option value="18.00-19.00">18.00-19.00</option>
                        <option value="19.00-20.00">19.00-20.00</option>
                        <option value="20.00-21.00">20.00-21.00</option>
                    </select>
                    @error('jam')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="jumlah">Jumlah Orang <span class="text-danger">*</span></label>
                    <input type="number" class="form-control @error('jumlah') is-invalid @enderror" name="jumlah" id="jumlah" min="1" max="8" placeholder="Masukkan jumlah orang" value="{{ old('jumlah') }}">
                    @error('jumlah')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-dark btn-block mb-3">Booking</button>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="{{ asset('js/reservasi.js')  }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
@endpush