@extends('layouts.main')

@section('title', 'Invoice')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/style.css')  }}">
@endpush

@section('content')
    <div class="container min-vh-100" style="margin-top: 84px">
        <h1 class="handwriting" style="text-align: left">Invoice</h1>
        <p class="lato" style="text-align: left">Silahkan melihat invoice transaksi yang sudah pernah dilakukan</p> <hr/>
        <div class="row">
            @foreach ($reservations as $reservation)
                <div class="col-12 col-md-6 col-lg-4 mb-5">
                    <div class="card" style="width: auto;">
                        <div class="card-body">
                            <h1 class="card-title handwriting" style="text-align: left">Invoice #{{ $loop->iteration }}</h1>
                            <table class="table">
                                <tbody>
                                    <tr class="card-subtitle mb-2 text-muted">
                                        <th><h6>Tanggal Reservasi</h6></th>
                                        <td class="idReservasi" style="display:none;">{{ $reservation->id }}</td>
                                        <td><h6>{{ date('d/m/Y', strtotime($reservation->tanggal )); }}</h6></td>
                                    </tr>
                                    <tr class="card-subtitle mb-2 text-muted">
                                        <th><h6>Tanggal Pemesanan</h6></th>
                                        <td><h6>{{ date('d/m/Y', strtotime($reservation->created_at )); }}</h6></td>
                                    </tr>
                                    <tr class="card-subtitle mb-2 text-muted">
                                        <th><h6>Biaya</h6></th>
                                        <td><h6>Rp. {{ number_format(50000, 2, ',', '.'); }}</h6></td>
                                    </tr>
                                </tbody>
                            </table>
                            <button data-toggle="modal" data-target="#invoiceModal" class="btn btn-outline-dark btn-block invoiceBtn">Lihat Invoice</button>
                        </div>
                    </div>
                </div>                
            @endforeach
        </div>
    </div>

    <footer class="dark-bg" style="position: relative; height: 54px;">
        <p class="center-text lato">Rumah Makan Pindang Ombai © 2021</p>
    </footer>
@endsection

@push('modal')
    <div class="modal fade" id="invoiceModal" tabindex="-1" aria-labelledby="invoiceModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-body">
                    <h1 class="handwriting" style="text-align: left">Invoice</h1><hr/>
                    <p class="lato" style="text-align: left; margin-bottom: 0px"><b>Kode Invoice:</b> <span id="kodeInvoice">INV-1</span></p>
                    <p class="lato" style="text-align: left"><b>Tanggal Invoice:</b> <span id="tanggalInvoice">24/04/2022</span></p> 
                    <table class="table" id="modalTable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Item</th>
                                <th scope="col">Jam</th>
                                <th scope="col">Jumlah Orang</th>
                                <th scope="col">Tanggal Reservasi</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="/api/invoice/bayar" method="post" id="formBayar">
                        @csrf
                        <input type="hidden" name="idReservasiHidden" id="idReservasiHidden" value="">
                        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="name" id="name" value="{{ auth()->user()->name }}">
                        <input type="hidden" name="email" id="email" value="{{ auth()->user()->email }}">
                        <button type="submit" class="btn btn-primary bayarBtn">Bayar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('script')
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('js/invoice.js')  }}"></script>
@endpush