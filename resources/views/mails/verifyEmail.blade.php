@component('mail::message')
Halo **{{$name}}**,  {{-- use double space for line break --}}
Terima kasih sudah melakukan pendaftaran!

Silahkan klik tombol dibawah untuk melanjutkan
@component('mail::button', ['url' => $link])
Verifikasi Email
@endcomponent
Dengan hormat,
Tim Pindang Ombai.
@endcomponent